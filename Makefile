.PHONY: all clean

P = loa
CC = gcc
CFLAGS = -Wall -Wextra -Wshadow -pedantic -O3 -std=c99 -march=native
CLIBS = -lpthread
DEBUG =
SRC = src/mainB.c src/getch.c src/grid.c src/maps.c src/player.c src/menu.c src/list.c src/enemy.c src/scenery.c src/entity.c src/randomizer.c
OBJ = ${SRC:.c=.o}

all: P

src/%.o: %.c
	${CC} -c ${CFLAGS} $<

P: ${OBJ}
	${CC} -o ${P} ${OBJ} ${CLIBS} ${DEBUG}

debug: DEBUG = -ggdb -fsanitize=thread -fno-omit-frame-pointer -Wunreachable-code -Wconversion -ftrapv -fsanitize=undefined

debug: clean P

clean:
	rm -f ${P}
	rm -f src/*.o
