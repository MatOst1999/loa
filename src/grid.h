#pragma once

#include "types.h"

void
grid_showcursor(void);

void
grid_draw(uint length, uint heigth);

uint
grid_x_conversion(uint pos);

uint
grid_y_conversion(uint pos);

void
grid_entity_print(entity_t *entity);

void
grid_entity_deprint(entity_t *entity);

void
grid_cursor_save();

void
grid_cursor_restore();

