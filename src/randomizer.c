#include <stdio.h>
#include <stdlib.h>

#include "globals.h"

char *keys = "ABCD";

char keyset[24][4];

void
permutate(void)
{
  int index = 0;

  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      if (j == i)
        continue;
      else {
        for (int k = 0; k < 4; k++) {
          if (k == j || k == i)
            continue;
          else {
            for (int l = 0; l < 4; l++) {
              if (l == k || l == j || l == i)
                continue;
              else {
                keyset[index][0] = keys[i];
                keyset[index][1] = keys[j];
                keyset[index][2] = keys[k];
                keyset[index][3] = keys[l];
                index++;
              }
            }
          }
        }
      }
    }
  }
}

void
change_keys()
{
  int value = rand() % 24;
  UP = keyset[value][0];
  DOWN = keyset[value][1];
  RIGHT = keyset[value][2];
  LEFT = keyset[value][3];
}
