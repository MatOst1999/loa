#include <stdlib.h>

#include "grid.h"
#include "player.h"
#include "types.h"

player_t *
player_new(int x, int y)
// Player/entity initialization
{
  player_t *player = calloc(1, sizeof(player_t));
  player->xpos = x;
  player->ypos = y;
  player->hp = 100;
  player->damage = 100000;
  grid_entity_print(player);
  player->facing = NORTH;
  return player;
}

void
player_update(player_t *player, uint newxpos, uint newypos, DIRECTION newfacing)
{
  grid_entity_deprint(player);
  player->xpos = newxpos;
  player->ypos = newypos;
  player->facing = newfacing;
  grid_entity_print(player);
}

void
player_destroy(player_t *player)
{
  grid_entity_deprint(player);
  free(player);
}
