#include <stdlib.h>

#include "defines.h"
#include "maps.h"

bool
map_is_tile_empty(tile_t *tile)
{
  if (tile)
    return (tile->entity == NULL);

  return false;
}

map_t
map_new(void)
{
  map_t map = calloc(XSIZE, sizeof(tile_t **));

  for (uint i = 0; i < XSIZE; ++i)
    map[i] = calloc(YSIZE, sizeof(tile_t *));

  for (uint i = 0; i < XSIZE; i++) {
    for (uint j = 0; j < YSIZE; j++) {
      map[i][j] = calloc(1, sizeof(tile_t));
      map[i][j]->entity = NULL;
    }
  }

  return map;
}

void
map_destroy(map_t map)
{
  for (uint i = 0; i < XSIZE; i++) {
    for (uint j = 0; j < YSIZE; j++)
      free(map[i][j]);

    free(map[i]);
  }

  free(map);
}

void *
map_add_entity(map_t map, entity_t *entity)
{
  tile_t *tile = map[entity->xpos][entity->ypos];
  tile->entity = entity;
  return NULL;
}

void *
map_remove_entity(map_t map, entity_t *entity)
{
  tile_t *tile = map[entity->xpos][entity->ypos];
  tile->entity = NULL;
  return NULL;
}

bool
map_is_in_bounds(int x, int y)
{
  return (x >= 0 && x < XSIZE && y >= 0 && y < YSIZE);
}

tile_t *
map_get_facing_tile(entity_t *entity, map_t map)
{
  int changex = 0;
  int changey = 0;
  int tilex = entity->xpos;
  int tiley = entity->ypos;

  switch (entity->facing) {
    case SOUTH:
      changex = 0;
      changey = 1;
      break;

    case WEST:
      changex = -1;
      changey = 0;
      break;

    case EAST:
      changex = 1;
      changey = 0;
      break;

    case NORTH:
      changex = 0;
      changey = -1;
      break;

    default:
      changex = 0;
      changey = 0;
  }

  tilex += changex;
  tiley += changey;
  tile_t *tile = NULL;

  if (map_is_in_bounds(tilex, tiley))
    tile = map[tilex][tiley];

  return tile;
}




