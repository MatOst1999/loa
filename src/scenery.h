#pragma once

#include "types.h"

scenery_t *testscene;

scenery_t *
scenery_new();

void
scenery_destroy(scenery_t *scene);

void
scenery_add_enemy(scenery_t *scene, enemy_data *data);

void *
scenery_update(scenery_t *scene);

void
scenery_updates(scenery_t *scene);

