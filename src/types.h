#pragma once

#include <pthread.h>
#include <semaphore.h>

typedef unsigned int uint;

typedef enum {NORTH, EAST, SOUTH, WEST} DIRECTION;

typedef enum {PLAYER_CLASS, ENEMY_CLASS, OTHER_CLASS} ENTITY_CLASS;

typedef enum {PLAYER_HEALTH, PLAYER_FACING, KEY_DATAUP, KEY_DATADOWN, KEY_DATALEFT, KEY_DATARIGHT, PLAYER_ATTACK} MENU_DATA;

typedef struct {
  uint xpos;
  uint ypos;
  uint hp;
  DIRECTION facing;
  uint damage;
  ENTITY_CLASS ent_class;
} player_t;

typedef player_t entity_t;

typedef struct {
  entity_t *entity;
} tile_t;

typedef tile_t ***map_t;

typedef enum {test, ENEMY_SPIRALER} ENEMY_TYPE;

typedef struct {
  entity_t *entity;
  ENEMY_TYPE type;
  int id;
  pthread_t *thread;
  sem_t *semaphore;
  sem_t *scene_semaphore;
} enemy_t;

typedef struct {
  uint xpos;
  uint ypos;
  ENEMY_TYPE type;
} enemy_data;

typedef struct enemy_node {
  enemy_t *enemy;
  struct enemy_node *next;
} enemy_node;

typedef enemy_node *enemy_list;

typedef struct {
  map_t map;
  enemy_list *list;
} scenery_t;

typedef struct {
  map_t map;
  enemy_t *enemy;
} bundle_t;
