#pragma once

#include "types.h"

enemy_t *
enemy_create(enemy_data *data);

void
enemy_destroy(enemy_t *enemy);

int
enemy_get_id(enemy_t *enemy);

void *
enemy_move(void *data);

void
enemy_send_signal_and_wait(enemy_t *enemy);

void
enemy_add_to_map(map_t map, enemy_t *enemy);

void
enemy_remove_from_map(map_t map, enemy_t *enemy);

uint
enemy_entity_get_hp(enemy_t *enemy);

