#pragma once

#include "types.h"

player_t *
player_new(int x, int y);

void
player_update(player_t *player, uint newxpos, uint newypos,
              DIRECTION newfacing);

void
player_destroy(player_t *player);

