#include <stdlib.h>

#include "enemy.h"
#include "globals.h"
#include "list.h"
#include "maps.h"
#include "scenery.h"

scenery_t *
scenery_new(void)
{
  // Scenery, list and map allocation
  scenery_t *scene = calloc(1, sizeof(scenery_t));
  scene->map = map_new();
  scene->list = list_new();
  return scene;
}

void
scenery_add_enemy(scenery_t *scene, enemy_data *data)
{
  enemy_list *list = scene->list;
  map_t map = scene->map;

  if (map_is_in_bounds(data->xpos, data->ypos)
      && map_is_tile_empty(map[data->xpos][data->ypos])) {
    // Only allocate and do stuff when its worth it
    // Set the global sem to 1
    enemy_t *enemy = enemy_create(data);

    // Allocate and do stuff if it succeded
    if (enemy) {
      // Load the global bundle with data
      pthread_mutex_lock(&g_bundlemutex);
      globalbundle.map = scene->map;
      globalbundle.enemy = enemy;
      // Add the enemy to the map and enemy list
      enemy_add_to_map(map, enemy);
      list_node_add(list, enemy);
      // Crete the enemy thread
      pthread_create(enemy->thread, NULL, enemy_move, (void *)&	globalbundle);
    }
  }
}

void
scenery_destroy(scenery_t *scene)
{
  map_destroy(scene->map);
  list_destroy(scene->list);
  free(scene);
}

void *
scenery_update(scenery_t *scene)
{
  map_t map = scene->map;
  // Traveling trough the list using triple reference
  enemy_list *list_tracer = scene->list;
  enemy_list node;
  // Adquiring the mutex of the scene
  pthread_mutex_lock(&scenerymutex);

  while (*list_tracer != NULL) {
    node = *list_tracer;
    // We remove the enemy from the map to update it
    enemy_remove_from_map(map, node->enemy);

    // If its dead erase it
    if (enemy_entity_get_hp(node->enemy) == 0) {
      *list_tracer = (*list_tracer)->next;
      list_node_destroy(node);
    } else {
      // Else send a signal to its thread so it moves and wait until it has moven
      enemy_send_signal_and_wait(node->enemy);
      // Read it to the map in the new updated position
      enemy_add_to_map(map, node->enemy);
      // Wait a litle
    }

    // Keep moving forward until it hits NULL
    if (*list_tracer != NULL)
      list_tracer = &((*list_tracer)->next);
  }

  // Releasing the mutex
  pthread_mutex_unlock(&scenerymutex);
  return NULL;
}
