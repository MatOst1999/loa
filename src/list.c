#include <stdlib.h>

#include "enemy.h"
#include "list.h"

enemy_node *
create_node()
{
  enemy_node *node = (enemy_node *)calloc(1, sizeof(enemy_node));
  return (node);
}

int
list_node_add(enemy_list *list, enemy_t *enemy)
{
  if (enemy == NULL)
    return 1;

  enemy_node *node = create_node();

  if (node == NULL)
    return -1;

  node->enemy = enemy;
  node->next = *list;
  *list = node;
  return enemy_get_id(node->enemy);
}

void
list_node_destroy(enemy_node *node)
{
  if (node) {
    enemy_destroy(node->enemy);
    free(node);
  }
}

int
list_node_remove(enemy_list *list, int fid)
{
  if (*list == NULL)
    return -1;

  enemy_list pre = *list;
  enemy_list act = *list;

  if ((*list)->enemy->id == fid) {
    (*list) = (*list)->next;
    list_node_destroy(pre);
    return 0;
  }

  act = act->next;

  while (act != NULL) {
    if (enemy_get_id(act->enemy) == fid) {
      pre->next = act->next;
      list_node_destroy(act);
      break;
    }

    act = act->next;
    pre = pre->next;
  }

  return 0;
}


enemy_list *
list_new(void)
{
  enemy_list *list = calloc(1, sizeof(enemy_list));
  return list;
}

void
list_destroy(enemy_list *list)
{
  enemy_node *aux = *list;
  enemy_node *node = *list;

  while (node != NULL) {
    node = node->next;
    list_node_destroy(aux);
    aux = node;
  }

  free(list);
}















