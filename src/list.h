#pragma once

#include "types.h"

int
list_node_add(enemy_list *list, enemy_t *enemy);

int
list_node_remove(enemy_list *list, int fid);

void
list_node_destroy(enemy_node *node);

enemy_list *
list_new();

void
list_destroy(enemy_list *list);

