#pragma once

#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>

#include "types.h"

bool ids[1000];

scenery_t *testscene;

map_t testmap;

bundle_t globalbundle;

player_t *player;

pthread_mutex_t cursormutex;
pthread_mutex_t scenerymutex;
pthread_mutex_t g_bundlemutex;
pthread_mutex_t keysmutex;

char UP;
char DOWN;
char RIGHT;
char LEFT;

sem_t bundles_sem;

