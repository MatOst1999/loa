#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>


#include "defines.h"
#include "entity.h"
#include "getch.h"
#include "globals.h"
#include "grid.h"
#include "maps.h"
#include "menu.h"
#include "player.h"
#include "scenery.h"
#include "randomizer.h"


void
debugfunctions(char c)
{
  switch (c) {
    case 'D':
    case 'd':
      damage(player, 1);
      break;

    case 'H':
    case 'h':
      heal(player, 1);
      break;

    case 'A':
    case 'a':
      entity_attack(player, testscene->map);
      break;

    default:
      break;
  }
}

void
gameplayloop()
{
  DIRECTION newfacing = NORTH;

  while (1) {
    int changex = 0, changey = 0;

    // Read characters inputed by players, if it's a escape sequence, read 2 times more to look for arrow keys
    char c = getch();
    pthread_mutex_lock(&keysmutex);

    if (c == '\033' && getch() == '[') {
      char key = getch();

      if (key == UP) {
        // Set vectors and facing based on the input
        changex = 0;
        changey = -1;
        newfacing = NORTH;
      } else
        if (key == DOWN) {
          changex = 0;
          changey = 1;
          newfacing = SOUTH;
        } else
          if (key == RIGHT) {
            changex = 1;
            changey = 0;
            newfacing = EAST;
          } else
            if (key == LEFT) {
              changex = -1;
              changey = 0;
              newfacing = WEST;
            } else
              newfacing = NORTH;
    } else
      if (c == 'x' || c == 'X')
        break;

    debugfunctions(c);
    int newxpos = player->xpos + changex;
    int newypos = player->ypos + changey;

    if (is_in_bounds(newxpos, newypos)
        && map_is_tile_empty(testmap[newxpos][newypos])) {
      // If is in bounds update position else just update facing
      map_remove_entity(testmap, player);
      player_update(player, newxpos, newypos, newfacing);
      map_add_entity(testmap, player);
    } else
      updatefacing(player, newfacing);

    pthread_mutex_unlock(&keysmutex);
    usleep(10000);
  }
}

void *
menuloop()
{
  while (1) {
    menu_update();
    usleep(10000);
  }
}


void *
scenery_loop()
{
  while (1) {
    scenery_update(testscene);
    usleep(1000000 / 2);
  }
}

void *
randomizer_loop()
{
  usleep(15000000);

  while (1) {
    pthread_mutex_lock(&keysmutex);
    change_keys();
    menu_update();
    key_layout_update();
    pthread_mutex_unlock(&keysmutex);
    usleep(15000000);
  }
}

int
main(void)
{
  hidecursor();
  struct termios old,new;
  tcgetattr(0,&old);
  new = old;
  new.c_lflag &= ~ECHO;
  tcsetattr(0, TCSANOW,&new); 
  // Declaration of threads
  srand(time(NULL));
  pthread_t menu, scenery, randomizer;
  UP = 'A';
  DOWN = 'B';
  RIGHT = 'C';
  LEFT = 'D';
  permutate();
  // Initialize some mutexes to fix concurrencies
  pthread_mutex_init(&cursormutex, NULL);
  pthread_mutex_init(&scenerymutex, NULL);
  pthread_mutex_init(&g_bundlemutex, NULL);
  pthread_mutex_init(&keysmutex, NULL);
  // Draw the grid
  grid_draw(XSIZE, YSIZE);
  // Lets start the scenery
  testscene = scenery_new();
  testmap =  testscene->map;
  // Temporal data to load enemies
  enemy_data data;
  data.type = ENEMY_SPIRALER;

  // Add some enemies to the scene
  for (uint i = 1; i <= 10; i++) {
    data.xpos = rand() % (18 + 1 - 1) + 1;
    data.ypos = rand() % (11 + 1 - 1) + 1;
    scenery_add_enemy(testscene, &data);
  }

  // Initializing the player struct and adding it to the map
  player = player_new(0, 0);
  map_add_entity(testmap, player);
  // Starting the menu
  menu_init();
  // Launch the scenery and menu update threads
  pthread_create(&menu, NULL, menuloop, NULL);
  pthread_create(&scenery, NULL, scenery_loop, NULL);
  pthread_create(&randomizer, NULL, randomizer_loop, NULL);
  // Start the gameplay
  gameplayloop();
  // Closing up
  pthread_cancel(menu);
  pthread_cancel(scenery);
  pthread_cancel(randomizer);
  pthread_join(menu, NULL);
  pthread_join(scenery, NULL);
  pthread_join(randomizer, NULL);
  pthread_mutex_destroy(&cursormutex);
  pthread_mutex_destroy(&scenerymutex);
  pthread_mutex_destroy(&g_bundlemutex);
  pthread_mutex_destroy(&keysmutex);
  player_destroy(player);
  scenery_destroy(testscene);
  tcsetattr(0, TCSANOW, &old);
  grid_showcursor();
  return 0;
}
