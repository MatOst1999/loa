#pragma once

#include <stdbool.h>

#include "types.h"

map_t testmap;

bool
map_is_tile_empty(tile_t *tile);

bool
map_is_in_bounds(int x, int y);

map_t
map_new();

void
map_destroy(map_t map);

void *
map_add_entity(map_t map, entity_t *entity);

void *
map_remove_entity(map_t map, entity_t *entity);

tile_t *
map_get_facing_tile(entity_t *entity, map_t map);


