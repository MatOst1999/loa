#include <stdio.h>

#include "defines.h"
#include "globals.h"
#include "grid.h"

void
grid_showcursor(){
 printf("\033[?25h");
 printf("\033[300D");
 printf("\033[300B"); 
} 

void
grid_cursor_save()
{
  pthread_mutex_lock(&cursormutex);
  printf("\033[s");
}

void
grid_cursor_restore()
{
  printf("\033[u");
  pthread_mutex_unlock(&cursormutex);
}


void
grid_draw(uint length, uint heigth)
{
  // Prints a grid using ASCII magic, dont ask me how it works because not even i know
  if (length > 0 && heigth > 0) {
    printf("\033[2J");
    printf("\033[1;1f");

    for (uint i = 0; i < length; i++) {
      printf(" ");

      for (uint j = 0 ; j < 8 * SQUARESIZE - 1; j++)
        printf("_");
    }

    printf("\n");

    for (uint i = 0; i < heigth ; i++) {
      for (uint j = 0; j < 4 * SQUARESIZE - 1; j++) {
        printf("|");

        for (uint k = 0 ; k < length; k++) {
          for (uint l = 0; l < SQUARESIZE; l++)
            printf("\t");

          printf("|");
        }

        printf("\n");
      }

      printf("|");

      for (uint j = 0; j < length; j++) {
        for (uint k = 0 ; k < 8 * SQUARESIZE - 1; k++)
          printf("_");

        printf("|");
      }

      printf("\n");
    }
  }
}

uint
grid_x_conversion(uint pos)
{
  return (8 * pos + 5) * SQUARESIZE;
}

uint
grid_y_conversion(uint pos)
{
  return (4 * pos + 3) * SQUARESIZE;
}

void
grid_entity_print(entity_t *entity)
{
  char c;
  uint xpos = grid_x_conversion(entity->xpos);
  uint ypos = grid_y_conversion(entity->ypos);
  grid_cursor_save();
  printf("\033[%d;%df", ypos, xpos);

  switch (entity->ent_class) {
    case PLAYER_CLASS:
      switch (entity -> facing) {
        case NORTH:
          c = '^';
          break;
        case WEST:
          c = '<';
          break;
        case EAST:
          c = '>';
          break;
        case SOUTH:
          c = 'v';
          break;
        default:
          break;
      }
      break;
    case ENEMY_CLASS:
      c = 'E';
      break;
    case OTHER_CLASS:
    default:
      c = 'O';
  }

  printf("%c", c);
  grid_cursor_restore();
}

void
grid_entity_deprint(entity_t *obstacule)
{
  uint xpos = grid_x_conversion(obstacule->xpos);
  uint ypos = grid_y_conversion(obstacule->ypos);
  grid_cursor_save();
  printf("\033[%d;%df", ypos, xpos);
  printf(" ");
  grid_cursor_restore();
}
