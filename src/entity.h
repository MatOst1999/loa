#pragma once

#include <stdbool.h>

#include "types.h"

bool
is_in_bounds(int x, int y);

uint
entity_get_hp(entity_t *entity);

void
updatefacing(player_t *player, DIRECTION newfacing);

void
damage(player_t *player, uint amount);

void
heal(player_t *player, uint amount);

bool
entity_can_be_damaged(entity_t *entity, tile_t *tile);

void
entity_attack(entity_t *attacker, map_t map);

void
entity_move_forward(entity_t *entity, map_t map);

void
entity_rotate(entity_t *entity, bool orientation);

