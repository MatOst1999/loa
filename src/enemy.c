#include <stdbool.h>
#include <stdlib.h>

#include "enemy.h"
#include "entity.h"
#include "globals.h"
#include "grid.h"
#include "maps.h"

int
enemy_get_id(enemy_t *enemy)
{
  return (enemy->id);
}

void
set_data(enemy_t *enemy, enemy_data *data)
{
  // Load the data from the enemy_data struct to the enemy struc
  enemy->entity->xpos = data->xpos;
  enemy->entity->ypos = data->ypos;
  enemy->entity->ent_class = ENEMY_CLASS;

  switch (data->type) {
    case test:
    default:
      enemy->entity->hp = 50;
      enemy->entity->damage = 5;
  }

  enemy->type = data->type;
}

enemy_t *
enemy_create(enemy_data *data)
{
  // Find and usable id and take it
  uint id = 0;

  while (id < 1000) {
    if (!ids[id])
      break;

    id++;
  }

  // If all ids are taken abort
  if (ids[id])
    return NULL;

  ids[id] = !ids[id];
  // Alloc the new enemy
  enemy_t *enemy = (enemy_t *)calloc(1, sizeof(enemy_t));

  // If error, abort
  if (!enemy) {
    ids[id] = !ids[id];
    return NULL;
  }

  // Allocate the thread of the enemy
  pthread_t *thread = (pthread_t *)calloc(1, sizeof(pthread_t));

  // If error, abort
  if (!thread) {
    ids[id] = !ids[id];
    free(enemy);
    return NULL;
  }

  // Allocate the semaphire of the enemy
  sem_t *sem = (sem_t *)calloc(1, sizeof(sem_t));

  // If error, abort
  if (!sem) {
    ids[id] = !ids[id];
    free(enemy);
    free(thread);
    return NULL;
  }

  // Alocate the enemy's scene semaphore
  sem_t *scene_sem = (sem_t *)calloc(1, sizeof(sem_t));

  // If error, abort
  if (!scene_sem) {
    ids[id] = !ids[id];
    free(enemy);
    free(thread);
    free(sem);
    return NULL;
  }

  // Allocate an entity
  entity_t *entity = (entity_t *)calloc(1, sizeof(entity_t));

  // If error, abort
  if (!entity) {
    ids[id] = !ids[id];
    free(thread);
    free(sem);
    free(scene_sem);
    free(enemy);
    return NULL;
  }

  // Asign all the new pointers to the struct and set the remaining data
  enemy->entity = entity;
  enemy->id = id;
  enemy->thread = thread;
  enemy->semaphore = sem;
  enemy->scene_semaphore = scene_sem;
  set_data(enemy, data);
  return enemy;
}

void
enemy_destroy(enemy_t *enemy)
{
  // Check for null
  if (enemy) {
    int id = enemy->id;
    ids[id] = !ids[id];

    if (enemy->entity)
      free(enemy->entity);

    // Close the enemy thread and free it
    pthread_cancel(*enemy->thread);
    pthread_join(*enemy->thread, NULL);
    free(enemy->thread);
    // Once the thread is closed, we destroy and free the semaphores
    sem_destroy(enemy->semaphore);
    free(enemy->semaphore);
    sem_destroy(enemy->scene_semaphore);
    free(enemy->scene_semaphore);
    // Free the pointer
    free(enemy);
  }
}

uint
enemy_entity_get_hp(enemy_t *enemy)
{
  return (entity_get_hp(enemy->entity));
}

void
enemy_add_to_map(map_t map, enemy_t *enemy)
{
  map_add_entity(map, enemy->entity);
  grid_entity_print(enemy->entity);
}

void
enemy_remove_from_map(map_t map, enemy_t *enemy)
{
  map_remove_entity(map, enemy->entity);
  grid_entity_deprint(enemy->entity);
}

void
enemy_send_signal_and_wait(enemy_t *enemy)
{
  // Initialize the semaphore for the enemy to comunicate with the scene
  sem_init(enemy->scene_semaphore, 0, 0);
  // Increment the thread specific semaphore
  sem_post(enemy->semaphore);
  // And wait til the thread let us move
  sem_wait(enemy->scene_semaphore);
  // Destroy the semaphore for later use
  sem_destroy(enemy->scene_semaphore);
}

void *
enemy_move(void *data)
{
  bundle_t *bundle = (bundle_t *) data;
  enemy_t *enemy = bundle->enemy;
  map_t map = bundle->map;
  int move_counter;
  pthread_mutex_unlock(&g_bundlemutex);

  switch (enemy->type) {
    case ENEMY_SPIRALER:
      move_counter = 4;
      int moves = 4;
      int steps = 0;

      while (1) {
        sem_wait(enemy->semaphore);

        if (moves == 0)
          moves = 4;
        else
          if (steps == 4 * moves) {
            moves--;
            steps = 0;
          } else
            if (!move_counter
                || !map_is_tile_empty(map_get_facing_tile(enemy->entity, map))) {
              entity_rotate(enemy->entity, false);
              move_counter = moves;
            } else {
              entity_move_forward(enemy->entity, map);
              move_counter--;
              steps++;
            }

        sem_post(enemy->scene_semaphore);
      }

    case test:
    default:
      move_counter = 4;

      while (1) {
        sem_wait(enemy->semaphore);

        if (!move_counter
            || !map_is_tile_empty(map_get_facing_tile(enemy->entity, map))) {
          entity_rotate(enemy->entity, true);
          move_counter = 4;
        } else {
          entity_move_forward(enemy->entity, map);
          move_counter--;
        }

        sem_post(enemy->scene_semaphore);
      }
  }

  return NULL;
}



