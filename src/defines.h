#pragma once

// Grid.c
#define SQUARESIZE 1
#define YSIZE 11/SQUARESIZE
#define XSIZE 18/SQUARESIZE

// Menu.c
#define MENUTABS 3
#define MENUSTARTY 7
#define MENUSTARTX 147
#define MENUENDX 147
#define MENUENDY (MENUSTARTY + 2*MENUTABS)
#define ATTACK_BUTTON 'A'

// Cursor cheats
#define hidecursor() printf("\033[?25l");
