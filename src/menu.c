#include <stdio.h>

#include "defines.h"
#include "menu.h"
#include "globals.h"
#include "grid.h"

MENU_DATA tabs[] = { PLAYER_FACING, PLAYER_HEALTH, PLAYER_ATTACK};

void
print_key_cell_draw()
{
  for (uint i = 0; i < 2; i++)
    printf("_");

  printf(" ");

  for (uint i = 0; i < 2; i++)
    printf("_");

  printf("\033[6D");

  for (uint i = 0; i < 3; i++) {
    printf("\033[1B");
    printf("|");
    printf("\033[5C");
    printf("|");
    printf("\033[7D");
  }

  printf("\033[1C");

  for (uint i = 0; i < 2; i++)
    printf("_");

  printf(" ");

  for (uint i = 0; i < 2; i++)
    printf("_");
}

void
print_key_layout(void)
{
  printf("\033[%d;%df", MENUENDY + 1, MENUENDX + 8);
  print_key_cell_draw();
  printf("\033[%d;%df", MENUENDY + 4, MENUENDX + 2);
  print_key_cell_draw();
  printf("\033[%d;%df", MENUENDY + 4, MENUENDX + 8);
  print_key_cell_draw();
  printf("\033[%d;%df", MENUENDY + 4, MENUENDX + 14);
  print_key_cell_draw();
}

void
clean_layout_square()
{
  for (uint i = 0; i < 2; i++) {
    for (uint j = 0; j < 3; j++)
      printf(" ");

    printf("\033[1B");
    printf("\033[3D");
  }

  printf("_ _");
}

void
print_key_value(char key)
{
  if (key == UP) {
    printf("|");
    printf("\033[1A");
    printf("\033[1D");
    printf("^");
  } else
    if (key == DOWN) {
      printf("|");
      printf("\033[1B");
      printf("\033[1D");
      printf("v");
    } else
      if (key == RIGHT)
        printf("->");

      else
        if (key == LEFT) {
          printf("-");
          printf("\033[2D");
          printf("<");
        } else
          printf(" ");
}

void
key_layout_update()
{
  grid_cursor_save();
  printf("\033[%d;%df", MENUENDY + 2, MENUENDX + 9);
  clean_layout_square();
  printf("\033[%d;%df", MENUENDY + 3, MENUENDX + 10);
  print_key_value('A');
  printf("\033[%d;%df", MENUENDY + 5, MENUENDX + 3);
  clean_layout_square();
  printf("\033[%d;%df", MENUENDY + 6, MENUENDX + 4);
  print_key_value('D');
  printf("\033[%d;%df", MENUENDY + 5, MENUENDX + 9);
  clean_layout_square();
  printf("\033[%d;%df", MENUENDY + 6, MENUENDX + 10);
  print_key_value('B');
  printf("\033[%d;%df", MENUENDY + 5, MENUENDX + 15);
  clean_layout_square();
  printf("\033[%d;%df", MENUENDY + 6, MENUENDX + 16);
  print_key_value('C');
  grid_cursor_restore();
}


void
menu_update()
{
  grid_cursor_save();

  // Set cursor and printf HP
  for (uint i = 0; i < MENUTABS; i++) {
    printf("\033[%d;%df", MENUSTARTY + 2 * i, MENUSTARTX);

    switch (tabs[i]) {
      case PLAYER_HEALTH:
        printf("-HP: %u", player->hp);

        for (uint i = 0; i < 10; i++)
          printf(" ");

        break;

      case PLAYER_FACING:
        printf("-FACING %c: ", 0x257f);

        switch (player->facing) {
          case NORTH:
            printf("NORTH");
            break;

          case SOUTH:
            printf("SOUTH");
            break;

          case EAST:
            printf("EAST");
            break;

          case WEST:
            printf("WEST");
            break;
        }

        for (uint i = 0; i < 10; i++)
          printf(" ");

        break;

      case PLAYER_ATTACK:
        printf("-ATTACK: %c",ATTACK_BUTTON);
        break;

      default:
        ;
    }
  }

  grid_cursor_restore();
}

void
menu_init()
{
  // Prints the menu header and does the first update
  grid_cursor_save();
  printf("\033[5;158f");
  printf("\033[4mMENU\033[0m");
  print_key_layout();
  grid_cursor_restore();
  menu_update();
  key_layout_update();
}
