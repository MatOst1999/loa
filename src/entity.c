#include "defines.h"
#include "entity.h"
#include "maps.h"

bool
is_in_bounds(int x, int y)
{
  return (x >= 0 && x < XSIZE && y >= 0 && y < YSIZE);
}

uint
entity_get_hp(entity_t *entity)
{
  return (entity->hp);
}

void
updatefacing(player_t *player, DIRECTION newfacing)
{
  player->facing = newfacing;
}

void
entity_rotate(entity_t *entity, bool orientation)
{
  DIRECTION newfacing;

  if (orientation)
    newfacing = (entity->facing + 1) % 4;
  else
    newfacing = (entity->facing - 1) % 4;

  updatefacing(entity, newfacing);
}

void
damage(player_t *player, uint amount)
{
  if (player->hp > amount)
    player->hp -= amount;
  else
    player->hp = 0;
}

void
heal(player_t *player, uint amount)
{
  if (player->hp + amount <= 100)
    player->hp += amount;
  else
    player->hp = 100;
}

bool
entity_can_be_damaged(entity_t *entity, tile_t *tile)
{
  if (!map_is_tile_empty(tile)) {
    ENTITY_CLASS cl = tile->entity->ent_class;

    switch (entity->ent_class) {
      case ENEMY_CLASS:
        return (cl == PLAYER_CLASS);
        break;

      case PLAYER_CLASS:
        return (cl == ENEMY_CLASS);
        break;

      case OTHER_CLASS:
      default:
        return false;
    }
  }

  return false;
}

void
entity_attack(entity_t *attacker, map_t map)
{
  tile_t *tile = map_get_facing_tile(attacker, map);

  if (tile && entity_can_be_damaged(attacker, tile))
    damage(tile->entity, attacker->damage);
}

void
entity_move_forward(entity_t *entity, map_t map)
{
  int changex;
  int changey;
  tile_t *tile = map_get_facing_tile(entity, map);

  switch (entity->facing) {
    case SOUTH:
      changex = 0;
      changey = 1;
      break;

    case WEST:
      changex = -1;
      changey = 0;
      break;

    case EAST:
      changex = 1;
      changey = 0;
      break;

    case NORTH:
      changex = 0;
      changey = -1;
      break;

    default:
      changex = 0;
      changey = 0;
  }

  if (tile && map_is_tile_empty(tile)) {
    entity->xpos += changex;
    entity->ypos += changey;
  }
}

